import { Injectable } from '@angular/core';
import { UserModel } from '../model/user-model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private userSet: UserModel[] = [];
  constructor() { }
  addUser(emailId: string, password: string){
    this.userSet.push({
      emailId,
      password
    });
    console.log('user data', this.userSet)
  }
  verifyUser(emailId: string, password: string){
    const userResult: UserModel = this.userSet.find((user: UserModel) => {
       return user.emailId.toLocaleUpperCase() === emailId.toLowerCase();
    });
    console.log('user name', userResult)
    if (userResult && userResult.password === password){
      return true;
    }
    return false;
  }
}
