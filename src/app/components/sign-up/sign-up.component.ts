import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  userForm = new FormGroup({
    emailId: new FormControl(''),
    password: new FormControl(''),
  });
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }
  onSubmit(){
    console.log('submit')
    console.log('user name', this.userForm.value.emailId)
    console.log('submit')
    this.authService.addUser(this.userForm.value.emailId, this.userForm.value.password);
  }

}
