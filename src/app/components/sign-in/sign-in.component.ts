import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {AuthService} from '../../services/auth.service'

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  userForm = new FormGroup({
    emailId: new FormControl(''),
    password: new FormControl(''),
  });
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }
  onSubmit(){

    const result = this.authService.verifyUser(this.userForm.value.emailId, this.userForm.value.password);
    console.log('result',result)
  }
}
